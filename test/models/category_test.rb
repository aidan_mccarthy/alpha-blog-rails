require 'test_helper'
# All the tests for categories
class CategoryTest < ActiveSupport::TestCase
  def setup
    @category = Category.new(name: 'sport')
  end

  test 'category should be valid' do
    assert @category.valid?
  end

  test 'name should be present' do
    @category.name = ''
    assert_not @category.valid?
  end

  test 'name should be unique' do
    @category.save
    category_not_unique = Category.new(name: 'sport')
    assert_not category_not_unique.valid?
  end

  test 'name should not be too short ' do
    @category.name = 'A'
    assert_not @category.valid?
  end

  test 'name should not be too long ' do
    @category.name = 'A' * 26
    assert_not @category.valid?
  end
end
