require 'test_helper'
# All the tests for categories
class ArticleCategoryControllerTest < ActionDispatch::IntegrationTest
  def setup
    @category = Category.create(name: 'sport')
    @user = User.create(username: 'user', email: 'user@user.com', password: 'password', admin: false)
    @article = Article.new(title: 'ABCDE', description: 'This is the description')
  end

  test 'test' do
    @article.user = @user
    @article.categories << @category
    assert @article.save
  end
end
