require 'test_helper'
# All the tests for categories
class CategoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @category = Category.create(name: 'sport')
    @admin = User.create(username: 'Admin', email: 'admin@admin.com', password: 'password', admin: true)
  end

  test 'should get categories index' do
    get categories_path
    assert_response :success
  end

  test 'should get new' do
    sign_in_as(@admin, 'password')
    get new_category_path
    assert_response :success
  end

  test 'should get show' do
    get categories_path(@category)
    assert_response :success
  end

  test 'should redirect create when not logged in as admin' do
    assert_no_difference 'Category.count' do
      post categories_path, params: { category: { name: 'sports' } }
    end
    assert_redirected_to categories_path
  end
end
